
const webpack = require('webpack');

module.exports = {  
  entry: './app.ts',
  output: {
	path: './dist/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  module: {
    loaders: [
		{ test: /\.nunj$/, loader: 'nunjucks-loader' },
		{ test: /\.(ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/, loader : 'url' },
		{ test: /\.css$/, loader: "style!css" },
        { test: /\.ts$/, loader: 'ts-loader' },
        { test: /\.html$/, loader: 'raw' }
    ]
  },
  plugins: [
	new webpack.ProvidePlugin({
		$: "jquery",
        jQuery: "jquery"
    }),
    new webpack.ProvidePlugin({
		Nedb: 'nedb'
    })
  ]
}
