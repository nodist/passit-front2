
import {Component} from 'angular2/core';
import {Router, RouteConfig, OnActivate} from 'angular2/router';
import Controller from '../controller/controller';

@Component({
    template: require('../template/login.html'),
    styleUrls: ['../css/login.css']
})

export default class Login {

    constructor(private _router:Router, private _controller:Controller){

    }

};
