
import {Component} from 'angular2/core';
import {Control, ControlGroup} from 'angular2/common';
import {Router, RouteConfig, OnActivate} from 'angular2/router';
import Controller from '../controller/controller';
import {emailControl, passwordControl, formGroup, placeholder} from '../include/form';

@Component({
    template: require('../template/signup.html'),
    styleUrls: ['../css/signup.css']
})

export default class Signup {

    /**
     * The email control
     * @type {Control}
     */
    public emailControl: Control = emailControl;

    /**
     * The password control
     * @type {Control}
     */
    public passwordControl: Control = passwordControl;

    /**
     * The form group
     * @type {Control}
     */
    public formGroup: ControlGroup = formGroup;

    /**
     * The placeholder
     * @type {placeHolder}
     */
    public placeholder: placeHolder = placeholder;

    /**
     * A lock to prevent mutiple signups
     * @type {boolean}
     */
    private _lock: boolean = false;

    constructor(private _router:Router, private _controller:Controller){

    }

    /**
     * Sign the user up
     */
    public signup(): void {
        if(this._lock){
            return;
        }
        this._lock = true;
        if(!this.formGroup.valid){
            this._lock = false;
            return this._controller.message.showStatus('Invalid Signup', 'Please enter your email address and password <br /> Passwords must be at least 6 charaters', 4000); 
        }
        this._controller.message.showStatus('Please Wait', 'We are preforming the first time setup'); 
        //check if the email exists
        this._controller.api.jsonGet(`/api/username-available/${this.formGroup.value.email}`).subscribe(res =>{
            //create the users keys
        }, err =>{
           this._controller.message.showStatus('Sign Up Error', 'This email has been used before, please use a different email address', 4000); 
           this._lock = false;
        });
    }

};
