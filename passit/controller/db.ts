
import {Injectable} from 'angular2/core';

@Injectable()
/**
 * Class for managing the localStorage db
 */
export default class Db {

    /**
     * The database for the passwords
     * @type {nedb}
     */
    public passwordDb: any;

    /**
     * The database for the groups
     * @type {nedb}
     */
    public groupDb: any;

    constructor(){
       this.passwordDb = new Nedb({filename:'passwords.db'});
       this.groupDb = new Nedb({filename:'groups.db'});
       this.passwordDb.loadDatabase();
       this.groupDb.loadDatabase();
    }

};
