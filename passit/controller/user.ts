
import {Injectable} from 'angular2/core';
import {createCookie, getCookie} from '../include/cookie';

@Injectable()
/**
 * The class for storing the user information
 */
export default class User {

    /**
     * The users info
     */
    private _stat: userInfo;

    constructor(){
        this.load();
    }

    /**
     * Get the user info
     * @return {userInfo} - the users info
     */
    public getInfo(): userInfo {
        return this._stat;
    }

    /**
     * Get a value from the stat
     * @param {string} name - the name of the property
     * @return {any} - the value
     */
    public get(name:string): any {
        return this._stat[name];
    }

    /**
     * Set a value on the stat
     * @param {string} name - the name of the property
     * @param {any} value - the value of the property
     */
    public set(name:string, value:any): void {
        this._stat[name] = value;
        this._save();
    }

    /**
     * Remove a value
     * @param {string} name - the name of the property
     */
    public remove(name:string): void {
        this._stat[name] = '';
        this._save();
    }

    /**
     * Load the users info from a cookie
     */
    public load(): void {
        var data = getCookie('user');
        if(!data){
            this._stat = {
                email: '',
                password: '',
                token: '',
                mode: ''
            };
        } else {
            this._stat = JSON.parse(atob(data));
        }
    }

    /**
     * Save the user info to a cookie
     * @param {number|null} days - the number of days to save the cookies or none for a session cookie
     */
    private _save(days?:number): void {
        if(this._stat){
            var data = btoa(JSON.stringify(this._stat));
            createCookie('user', data, days);
        }
    }

};
