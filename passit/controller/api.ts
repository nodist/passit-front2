
import {Injectable} from 'angular2/core';
import {Http, Headers} from 'angular2/http';
import User from './user';

@Injectable()
/**
 * Class for connection to the api
 */
export default class Api {

    /**
     * The base url of the api
     * @type {string}
     */
    private _baseUrl: string = 'http://0.0.0.0:8000';

    /**
     * Init
     * @param {Http} http - the angular2 http class
     * @param {User} user - the information about the current user
     */
    constructor(private _http:Http, private _user:User){

    }

    /**
     * Make a json post
     * @param {string} uri - the url of the to post to append to the base url
     * @param {object} body - the json body
     * @return {Promise} the http response
     */
    jsonPost(uri:string, body:any): any {
        var headers = new Headers();
        headers.append('Content-Type','application/json');
        headers.append('Accept','application/json');
        return this._http.post(this._baseUrl + uri, JSON.stringify(body), {headers:headers})
    }

    /**
     * Make a json get request
     * @param {string} uri - the url of the get request
     * @return {Promise} the http response
     */
    jsonGet(uri:string): any {
        var headers = new Headers();
        headers.append('Accept','application/json');
        return this._http.get(this._baseUrl + uri, {headers:headers});
    }

};
