
import {Control, ControlGroup, Validators} from 'angular2/common';

/**
 * The form control for the login and signup
 */

/**
 * Validate a users email
 * @param {Control} control - the control group to validate
 * @return {boolean} if the email is valid
 */
var emailValidator = (control: Control): {[key: string]: boolean} => {
    var re: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(control.value) ? null : {"Invalid Email": true}
};

/**
 * The form control for the email
 * @type {Control}
 */
export var emailControl: Control = new Control('', emailValidator);

export var passwordControl: Control = new Control('', Validators.minLength(6));

export var formGroup: ControlGroup = new ControlGroup({
    email: emailControl,
    password: passwordControl
});

export var placeholder: placeHolder = {
    email: 'Your Email',
    password: 'Your Password'
};
