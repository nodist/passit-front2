
import {Component} from 'angular2/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from 'angular2/router';
import Controller from './controller/controller';
import Login from './routes/login';
import Signup from './routes/signup';

@Component({
    selector:'app',
    template: require('./template/passit.html'),
    styleUrls: ['../css/passit.css'],
    directives:[ROUTER_DIRECTIVES]
})
@RouteConfig([
    { path:'/login', component:Login, name:'Login', useAsDefault: true },
    { path:'/signup', component:Signup, name:'Signup'}
])
export default class Passit {

    /**
     * The user info to pass to the template
     * @type {userInfo}
     */
    public stat: userInfo = this._controller.user.getInfo();

    constructor(private _router:Router, private _controller:Controller){

    }

};
