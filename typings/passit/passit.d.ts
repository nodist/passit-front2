
/**
 * The file is used for the definitions for the vars in passit
 */

/**
 * The nedb class
 * @type {nedb}
 */
declare var Nedb:any;

/**
 * Jquery
 * @type {jQuery}
 */
declare var $: any;

/**
 * The user info
 */
interface userInfo {
    /**
     * The email of the user
     * @type {string}
     */
    email:string

    /**
     * The users password
     * @type {string}
     */
    password: string

    /**
     * The token for connecting to the api
     * @type {string}
     */
    token: string,

    /**
     * The current mode of the system online, offline
     * @type {string}
     */
    mode: string

}

/**
 * Placeholder fields for the login and signup
 */
interface placeHolder {
    /**
     * The email placeholder
     * @type {string}
     */
    email: string,
    
    /**
     * The password placeholder
     * @type {string}
     */
    password: string

}

/**
 * The template object for the message templates
 */
interface messageTemplateObject {
    /**
     * The title of the message
     * @type {string}
     */
    title: string,

    /**
     * The body of the message
     * @type {string}
     */
    body: string,

    /**
     * The current id of the modal to prevent colisions
     * @type {string}
     */
    modalId: string,

    /**
     * If the message can be closed by the user
     * @type {boolean}
     */
    close?: boolean

}
